# ECS | Employee Self Service Website

## Description
This repository contains the frontend code for the Equitable Computer Services, Inc. Employee Self Service website. The website is developed using React JS.

## Cloning the Repository
To clone this repository, go to your preferred path of the file or create your own folder and run the following command in your terminal:

```bash
git clone https://gitlab.com/smacintern/ess_website.git
```

## Running the Code
Before running the code, make sure you have installed react-scripts. If not, you can install it by running:

```bash
npm install react-scripts
```

Once react-scripts is installed, you can run the code using:

```bash
npm start
```

## Support
If you need any assistance or have any questions, feel free to open an issue in this repository or reach out via email at smacintern@equicom.com.

## Project Status
This repository contains the frontend code for the Equitable Computer Services, Inc. Employee Self Service website. Please note that it is a work in progress and not yet completed.