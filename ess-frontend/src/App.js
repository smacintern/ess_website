import React from "react";
import "./index.css";
import SignIn from "./routes/SignInPage";
import ForgotPassword from "./routes/ForgotPasswordPage";
import Home from "./routes/nav-tabs/HomePage";
import { Route, Routes } from "react-router-dom";
import NEOPage from "./routes/nav-tabs/NEOPage";
import FAQsPage from "./routes/nav-tabs/FAQsPage";
import PersonalInformationPage from "./routes/sidebar-tabs/PersonalInformationPage";
import AttendancePage from "./routes/sidebar-tabs/AttendancePage";
import DTRPage from "./routes/sidebar-tabs/DTRPage";
import ViewListsPage from "./routes/sidebar-tabs/ViewListsPage";
import RequestsPage from "./routes/sidebar-tabs/RequestsPage";
import FormsPage from "./routes/sidebar-tabs/FormsPage";
import Administrator from "./routes/sidebar-tabs/Administrator";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<SignIn />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        {/*Navbar Screens*/}
        <Route path="/home" element={<Home />} />
        <Route path="/neo" element={<NEOPage />} />
        <Route path="/faqs" element={<FAQsPage />} />
        {/*Sidebar Screens*/}
        <Route path="/personal-information" element={<PersonalInformationPage />} />
        <Route path="/attendance" element={<AttendancePage />} />
        <Route path="/daily-time-record" element={<DTRPage />} />
        <Route path="/view-lists" element={<ViewListsPage />} />
        <Route path="/requests" element={<RequestsPage />} />
        <Route path="/forms" element={<FormsPage />} />
        <Route path="/administrator" element={<Administrator />} />
      </Routes>
    </>
  );
}

export default App;
