import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import companyLogo from "../assets/logo/ECS_Logo.png";
import backgroundImage from "../assets/others/ess-bg.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "./SignInStyle.css";

const SignInPage = () => {
  const navigate = useNavigate();
  
  const handleSignIn = () => {
    navigate("/home");
  };

  return (
    <div
      className="signin-screen"
      style={{ backgroundImage: `url(${backgroundImage})` }}
    >
      <Container fluid className="parent-container">
        <Row noGutters>
          {/* Left Container */}
          <Col md={5}>
            <div className="left-container">
              <img src={companyLogo} alt="Company Logo" className="logo" />
              <p className="tagline">
                Navigate your work life with
                <br /> confidence
              </p>
              <h2 className="sign-in-title">Sign In</h2>

              <Form>
                <Form.Group controlId="formUsername">
                  <Form.Control
                    type="text"
                    placeholder="Username"
                    className="input-box username"
                    aria-label="Username"
                  />
                </Form.Group>

                <Form.Group controlId="formPassword">
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    className="input-box password"
                    aria-label="Password"
                  />
                </Form.Group>

                <div className="checkbox-forgot-wrapper">
                  <Form.Group
                    controlId="formBasicCheckbox"
                    className="remember-me"
                  >
                    <Form.Check type="checkbox" label="Remember Me" />
                  </Form.Group>
                  <Link to="/forgot-password" className="forgot-password">
                    Forgot Password?
                  </Link>
                </div>

                <Button
                  variant="primary"
                  className="sign-in-button"
                  onClick={handleSignIn}
                >
                  Sign In
                </Button>
              </Form>
            </div>
          </Col>

          {/* Right Container */}
          <Col md={7}>
            <div className="right-container">
              <h2 className="ess-title">EMPLOYEE SELF-SERVICE</h2>
              <p className="ess-tagline">
                Take control of your work life by accessing your personal
                records and administrative tasks with ease.
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default SignInPage;
