import React from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import companyLogo from "../assets/logo/ECS_Logo.png";
import backgroundImage from "../assets/others/ess-bg.png";
import backIcon from "../assets/others/back-icon.png";
import "bootstrap/dist/css/bootstrap.min.css";
import "./ForgotPasswordStyle.css";

const ForgotPasswordPage = () => {
  return (
    <div
      className="forgotpass-screen"
      style={{ backgroundImage: `url(${backgroundImage})` }}
    >
      <Container fluid className="parent-container">
        <Row noGutters>
          {/* Left Container */}
          <Col md={5}>
            <div className="left-container">
              <img src={companyLogo} alt="Company Logo" className="logo" />
              <p className="tagline">
                Navigate your work life with<br></br> confidence
              </p>
              <h2 className="forgotpass-title">Forgot Password</h2>
              <Form>
                <Form.Group controlId="formUsername">
                  <Form.Control
                    type="text"
                    placeholder="Username"
                    className="input-box username"
                    aria-label="Username"
                  />
                </Form.Group>
                <Button variant="primary" className="email-button">
                  Send to Email
                </Button>
              </Form>
              <div className="back-link">
                <Link to="/">
                  <img src={backIcon} alt="Back" className="back-icon" />
                  {/* Back icon */}
                </Link>
              </div>
            </div>
          </Col>

          {/* Right Container */}
          <Col md={7}>
            <div className="right-container">
              <h2 className="ess-title">EMPLOYEE SELF-SERVICE</h2>
              <p className="ess-tagline">
                Take control of your work life by accessing your personal
                records and administrative tasks with ease.
              </p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ForgotPasswordPage;
