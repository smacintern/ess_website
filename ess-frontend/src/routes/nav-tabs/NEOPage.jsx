import React, { useState } from "react";
import "./styles/NEOStyle.css";
import detailsIcon from "../../assets/others/details.png";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import NEOModal from "../../components/modals/NEODetails";

const NEOPage = () => {
  const [showModal, setShowModal] = useState(false);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <div className="neo-screen">
      <div className="nav-bar">
        <Navbar />
      </div>
      <div className="side-bar">
        <Sidebar />
      </div>
      <div className="neo-container">
        <div className="rectangle-header" />
        <h2 className="neo-title">NEW EMPLOYEE ORIENTATION</h2>
        <img
          src={detailsIcon}
          alt="Details"
          className="details-icon"
          onClick={toggleModal}
        />
        <div className="video-container">
          <iframe
            className="video-iframe"
            src="https://www.youtube.com/embed/A6L1SKpMyhU"
            allowfullscreen
          ></iframe>
        </div>
        <NEOModal show={showModal} onClose={toggleModal} />
      </div>
      <div className="footer">
        <Footer />
      </div>
    </div>
  );
};

export default NEOPage;
