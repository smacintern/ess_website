import React from "react";
import "./styles/FAQsStyle.css";
import { Form, Button } from "react-bootstrap";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import faqsImage from "../../assets/others/faqs-illustration.png";
import FAQQuestion from "../../components/faq-components/FAQsQuestions"; 

const FAQsPage = () => {
  return (
    <div className="faqs-screen">
      <div className="nav-bar">
        <Navbar />
      </div>
      <div className="side-bar">
        <Sidebar />
      </div>
      <div className="faqs-content">
        <div className="left-faqs">
          <FAQQuestion
            question="Do I need to ask HR to change/edit or add some information on my Personal Information?"
            answer1="No. You can change/edit or add some information on your own however it needs an approval from their assigned Approver."
            subquestionTitle="Sub-Question"
            subquestion="How can I change/edit or add information?"
            subanswer1="Step 1: Go to Personal Files Menu >> Personal Information."
            subanswer2="Step 2: Change/Edit or add information needed."
            subanswer3="Step 3: Enter the reason for change. (Required Field)"
            subanswer4="Step 4: Click Submit Button."
          />
          
          <FAQQuestion
            question="How can I view my Leave Status?"
            answer1="Step 1: Go to Personal Files Menu >> Leave Status."
            answer2="Step 2: Then Leave Available window will appear."
          />
          <FAQQuestion
            question="How can I change my Daily Time Record?"
            answer1="Step 1: Go to Daily Time Record."
            answer2="Step 2: Click Payperiod Button >>> Choose the Payperiod then select the View button."
            answer3="Step 3: Select Edit button base on the Date wants to change the DTR"
            answer4="Step 4: Enter the correct Time in and out as well the reason and then Click the Save button"
          />
          <FAQQuestion
            question="How to change my password?"
            answer1="Step 1: Go to Personal Information Menu >> Change Password."
            answer2="Step 2: Enter the New Password and confirm it."
            answer3="Step 3: Then click Change Password Button."
          />
          <FAQQuestion
            question="How can I file my Official Business/es?"
            answer1="Step 1: Go to Requests Menu >> Official Business."
            answer2="Step 2: Enter appropriate details of the form"
            answer3="Step 3: Then click Submit button."
          />
          <FAQQuestion
            question="How can I file my Leave/s?"
            answer1="Step 1: Go to Requests Menu >> Leave."
            answer2="Step 2: Select and enter the necessary details of the Leave form."
            answer3="Step 3: Then click Submit button."
          />
          <FAQQuestion
            question="How can I file my Over Time?"
            answer1="Step 1: Go to Requests Menu >> Overtime."
            answer2="Step 2: Select and enter the necessary details of the Overtime form."
            answer3="Step 3: Then click Submit button."
          />
          <FAQQuestion
            question="Can I know my Submitted Transactions' Status? How?"
            answer1="Step 1: Go to To View Lists Menu"
            answer2="Step 2: Select the Document Type."
            answer3="Step 3: Choose if the file wants to view are For Approval, Approved, Cancelled or Disapproved."
            answer4="Step 4: Click Display button"
          />
          <FAQQuestion
            question="Can I cancel my Submitted Transactions? How?"
            answer1="Step 1: Go to To View Lists Menu"
            answer2="Step 2: Select the Document Type."
            answer3="Step 3: Tick the for Approval button >>> Tick the box beside the document wants to cancel."
            answer4="Step 4: Click Cancel Selected Request button."
          />
          <FAQQuestion
            question="Can I know the Approvers of my submitted transaction? How?"
            answer1="Yes, just follow the steps below."
            answer2="Step 1: Go to View Lists Menu."
            answer3="Step 2: Select the Document Type >>> Choose if the file wants to view are For Approval, Approved, Cancelled or Disapproved."
            answer4="Step 4: Click Display button >>> Click Details Button for specific document you want to view"
          />
          <FAQQuestion
            question="How can I file one and half day of leave?"
            answer1="Submit two leave documents. One is for whole day and the other for half day."
          />
        </div>
        <div className="right-faqs">
          <div className="top-section">
            <h2>Any Questions?</h2>
            <p>You can ask anything you want to know</p>
          </div>
          <div className="bottom-section">
            <h4>Let us know</h4>
            <Form>
              <Form.Group controlId="formQuestion">
                <Form.Control
                  as="textarea"
                  rows={5}
                  placeholder="Type your question here..."
                />
              </Form.Group>
              <Button variant="primary">Send</Button>
            </Form>
            <img src={faqsImage} alt="Illustration" />
          </div>
        </div>
      </div>
      <div className="footer">
        <Footer />
      </div>
    </div>
  );
};

export default FAQsPage;
