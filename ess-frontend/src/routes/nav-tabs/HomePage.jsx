import React from "react";
import "./styles/HomeStyle.css";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import BannerCarousel from "../../components/home-components/BannerCarousel";
import ListSummary from "../../components/home-components/ListSummary";
import ListInformation from "../../components/home-components/ListInformation";

const HomePage = () => {
  return (
    <div className="homepage-screen">
      <div className="nav-bar">
        <Navbar />
      </div>
      <div className="side-bar">
        <Sidebar />
      </div>
      <div className="homepage-content">
        <div className="banner-carousel">
          <div className="carousel-container">
            <div className="rectangle-header" />
            <BannerCarousel />
          </div>
        </div>
        <div className="bottom-content">
          <div className="list-summary-container">
            <div className="rectangle-header"/>
            <div>
              <ListSummary />
            </div>
          </div>
          <div className="list-information-container">
            <div className="rectangle-header" />
            <ListInformation />
          </div>
        </div>
      </div>
      <div className="footer">
        <Footer />
      </div>
    </div>
  );
};

export default HomePage;
