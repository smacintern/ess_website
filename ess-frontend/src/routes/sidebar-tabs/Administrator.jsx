import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import proceedIcon from "../../assets/others/proceed.png";
import "./styles/AdministratorStyle.css";

const Administrator = () => {
  return (
    <React.Fragment>
      <div className="administrator-screen">
        <div className="nav-bar">
          <Navbar />
        </div>
        <div className="side-bar">
          <Sidebar />
        </div>
        <div className="administrator-container">
          <Container fluid>
            <Row className="admin-item-container">
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>User Maintenance</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Maintenance Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Transfer Approval</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Transfer Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Announcements</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Announcements Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className="admin-item-container">
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Pay period</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Pay Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Reset Password</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Reset Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Holiday</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Holiday Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className="admin-item-container">
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>File Upload</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="File Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Shift Upload</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Shift Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={10}>
                        <span>View Leave Balance</span>
                      </Col>
                      <Col md={2}>
                        <img src={proceedIcon} alt="Balance Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className="admin-item-container">
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={10}>
                        <span>Manual Leave Application</span>
                      </Col>
                      <Col md={2}>
                        <img src={proceedIcon} alt="Manual Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Cancel Leave</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Cancel Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
              <Col md={4}>
                <div className="admin-item">
                  <div className="rectangle-header" />
                  <div className="admin-contents">
                    <Row>
                      <Col md={9}>
                        <span>Reports</span>
                      </Col>
                      <Col md={3}>
                        <img src={proceedIcon} alt="Reports Icon" />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
        <div className="footer">
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Administrator;
