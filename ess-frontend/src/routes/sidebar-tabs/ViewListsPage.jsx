import React from 'react'
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import "./styles/ViewListsStyle.css";

const ViewListsPage = () => {
  return (
    <React.Fragment>
      <div className="viewlists-screen">
        <div className="nav-bar">
          <Navbar />
        </div>
        <div className="side-bar">
          <Sidebar />
        </div>
        <div className="viewlists-container">
          <div className="viewlists-document-container">
            <div className="rectangle-header" />
            <div className="document-component">

            </div>
          </div>
          <div className='container-divider'></div>
          <div className="submitted-document-container">
            <div className="document-table-component">

            </div>
          </div>
        </div>
        <div className="footer">
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
}

export default ViewListsPage