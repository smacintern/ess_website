import React from "react";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import "./styles/AttendanceStyle.css";

const AttendancePage = () => {
  return (
    <React.Fragment>
      <div className="attendance-screen">
        <div className="nav-bar">
          <Navbar />
        </div>
        <div className="side-bar">
          <Sidebar />
        </div>
        <div className="attendance-container">
          <div className="attendance-log-container">
            <div className="rectangle-header" />
            <div className="attendance-component"></div>
          </div>
          
          <div className="leave-status-container">
            <div className="rectangle-header" />
            <div className="leave-component"></div>
          </div>
        </div>
        <div className="footer">
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
};

export default AttendancePage;
