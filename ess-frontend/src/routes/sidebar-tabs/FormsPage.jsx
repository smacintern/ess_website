import React from "react";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import "./styles/FormsStyle.css";
// import AccordionForms from "../../components/forms-components/AccordionForms";

const FormsPage = () => {
  return (
    <React.Fragment>
      <div className="forms-screen">
        <div className="nav-bar">
          <Navbar />
        </div>
        <div className="side-bar">
          <Sidebar />
        </div>
        <div className="forms-container">
          <div className="rectangle-header" />
          <h2 className="forms-title">Employee Resources</h2>
          <p className="forms-tagline">Access Forms for your Needs</p>

          <div className="accordion-container">
            <div className="left-accordion">{/* <AccordionForms /> */}</div>
            <div className="right-accordion"></div>
          </div>
        </div>

        <div className="footer">
          <Footer />
        </div>
      </div>
    </React.Fragment>
  );
};

export default FormsPage;
