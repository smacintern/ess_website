import React from "react";
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";
import WorkInformation from "../../components/personalinfo-components/WorkInformation";
import EmployeeInformation from "../../components/personalinfo-components/EmployeeInformation";
import PersonalHistory from "../../components/personalinfo-components/PersonalHistory";
import "./styles/PersonalInformationStyle.css";

const PersonalInformationPage = () => {
  return (
    <div className="personalinfo-screen">
      <div className="nav-bar">
        <Navbar />
      </div>
      <div className="side-bar">
        <Sidebar />
      </div>
      <div className="personalinfo-content">
        <div className="left-personalinfo">
          <div className="personalinfo-container">
            <div className="rectangle-header" />
            <WorkInformation />
          </div>
          <div className="personalinfo-container">
            <div className="rectangle-header" />
            <div className="employee-information">
              <EmployeeInformation />
            </div>
          </div>
        </div>
        <div className="right-personalinfo">
          <div className="personalinfo-container1">
            <div className="rectangle-header" />
            <div className="personal-history">
              <PersonalHistory />
            </div>
          </div>
        </div>
      </div>

      <div className="footer">
        <Footer />
      </div>
    </div>
  );
};

export default PersonalInformationPage;
