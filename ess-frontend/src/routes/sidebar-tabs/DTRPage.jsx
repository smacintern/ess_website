import React from 'react'
import Navbar from "../../components/NavBar";
import Sidebar from "../../components/Sidebar";
import Footer from "../../components/Footer";

const DTRPage = () => {
  return (
    <React.Fragment>
      <div className="nav-bar">
        <Navbar />
      </div>
      <div className="side-bar">
        <Sidebar />
      </div>
      <div className="footer">
        <Footer />
      </div>
    </React.Fragment>
  );
}

export default DTRPage