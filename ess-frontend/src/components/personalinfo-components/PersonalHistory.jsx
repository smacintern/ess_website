import React from "react";
import "./PersonalHistoryStyle.css";
import { Table } from "react-bootstrap";

const PersonalHistory = () => {
  const positionHistoryData = [
    { position: "Software Engineer", department: "SDS", date: "06/15/2024" },
    { position: "Intern", department: "SMAC", date: "03/20/2024" },
  ];

  const dependentsData = [
    {
      name: "Maria Doe",
      relationship: "Spouse",
      gender: "Female",
      birthdate: "01/01/1981",
    },
    {
      name: "Jane Doe",
      relationship: "Child",
      gender: "Female",
      birthdate: "04/06/2001",
    },
  ];

  const educationalBackgroundData = [
    {
      degree: "Elementary",
      school: "Corinthians Christian Academy",
      year: "2008 - 2014",
    },
    {
      degree: "Jr. High School",
      school: "Ramon Magsaysay Cubao High School",
      year: "2014 - 2018",
    },
    {
      degree: "Sr. High School",
      school: "New Era University",
      year: "2018 - 2020",
    },
    {
      degree: "Collage",
      school: "Technological Institute of the Philippines - Quezon City",
      year: "2020 - Present",
    },
  ];

  return (
    <div>
      <h3 className="bginfo-title">Position History</h3>
      <div className="table-divider"></div>
      <Table striped bordered className="bg-info">
        <thead>
          <tr>
            <th>Effectivity Date</th>
            <th>Department</th>
            <th>Position</th>
          </tr>
        </thead>
        <tbody>
          {positionHistoryData.map((item) => (
            <tr key={item.id}>
              <td>{item.date}</td>
              <td>{item.department}</td>
              <td>{item.position}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      <h3 className="bginfo-title">Dependents</h3>
      <div className="table-divider"></div>
      <Table striped bordered className="bg-info">
        <thead>
          <tr>
            <th>Relationship</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Birth Date</th>
          </tr>
        </thead>
        <tbody>
          {dependentsData.map((item) => (
            <tr key={item.id}>
              <td>{item.relationship}</td>
              <td>{item.name}</td>
              <td>{item.gender}</td>
              <td>{item.birthdate}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      <h3 className="bginfo-title">Educational Background</h3>
      <div className="table-divider"></div>
      <Table striped bordered className="bg-info">
        <thead>
          <tr>
            <th>Education</th>
            <th>School</th>
            <th>Year</th>
          </tr>
        </thead>
        <tbody>
          {educationalBackgroundData.map((item) => (
            <tr key={item.id}>
              <td>{item.degree}</td>
              <td>{item.school}</td>
              <td>{item.year}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default PersonalHistory;
