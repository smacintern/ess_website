import React, { useState } from "react";
import { Container, Button, Row, Col } from "react-bootstrap";
import defaultProfilePicture from "../../assets/sidebar-icons/user.png";
import ProfilePictureModal from "../modals/EditProfilePicture";
import ChangePasswordModal from "../modals/ChangePassword";
import editProfileIcon from "../../assets/others/editIcon.png";
import "./WorkInformationStyle.css";

const WorkInformation = () => {
  const [showProfilePictureModal, setShowProfilePictureModal] = useState(false);
  const [showChangePasswordModal, setShowChangePasswordModal] = useState(false);
  const [profilePicture, setProfilePicture] = useState(defaultProfilePicture);
  const [selectedFile, setSelectedFile] = useState(null);

  const handleShowProfilePictureModal = () => setShowProfilePictureModal(true);
  const handleCloseProfilePictureModal = () =>
    setShowProfilePictureModal(false);

  const handleShowChangePasswordModal = () => setShowChangePasswordModal(true);
  const handleCloseChangePasswordModal = () =>
    setShowChangePasswordModal(false);

  const handleProfilePictureChange = (event) => {
    const file = event.target.files[0];
    setProfilePicture(URL.createObjectURL(file));
    setSelectedFile(file);
  };

  const handleSaveProfilePicture = () => {
    console.log("Profile picture saved:", selectedFile);
    handleCloseProfilePictureModal();
  };

  const employeeName = "John Doe";
  const position = "Software Developer";
  const department = "Engineering";
  const email = "john.doe@example.com";
  const contact = "+1 234 567 890";
  const hireDate = "January 1, 2020";
  const workedFor = "2 years";
  const employeeID = "EMP12345";

  return (
    <Container fluid className="work-information-container">
      <Container className="details-top-container">
        <Container className="edit-profile-container">
          <img src={profilePicture} alt="Profile" className="profile-picture" />
          <img
            src={editProfileIcon}
            alt="edit-profile"
            className="edit-profilepic-icon"
            onClick={handleShowProfilePictureModal}
          />
        </Container>

        <Container className="employee-details">
          <div className="top-details">
            <h2>{employeeName}</h2>
            <Button
              variant="primary"
              className="change-password-button"
              onClick={handleShowChangePasswordModal}
            >
              Change Password
            </Button>
          </div>

          <ul>
            <li>
              <Container className="detail-item">
                <div className="detail-label1">
                  <strong>Position:</strong>
                </div>
                <span>{position}</span>
              </Container>
            </li>
            <li>
              <Container className="detail-item">
                <div className="detail-label2">
                  <strong>Department:</strong>
                </div>
                <span>{department}</span>
              </Container>
            </li>
            <li>
              <Container className="detail-item">
                <div className="detail-label3">
                  <strong>Email:</strong>
                </div>
                <span>{email}</span>
              </Container>
            </li>
            <li>
              <Container className="detail-item">
                <div className="detail-label4">
                  <strong>Contact:</strong>
                </div>
                <span>{contact}</span>
              </Container>
            </li>
          </ul>
        </Container>
      </Container>

      <Container fluid className="work-data">
        <Row className="work-data-row">
          <Col md={5} className="employee-work-data">
            <p>
              <strong>Hire Date</strong> {hireDate}
            </p>
          </Col>
          <Col md={4} className="employee-work-data">
            <p>
              <strong>Worked For</strong> {workedFor}
            </p>
          </Col>
          <Col className="employee-work-data">
            <p>
              <strong>Employee ID</strong> {employeeID}
            </p>
          </Col>
        </Row>
      </Container>

      {/* Modals */}
      <ProfilePictureModal
        showModal={showProfilePictureModal}
        handleCloseModal={handleCloseProfilePictureModal}
        profilePicture={profilePicture}
        handleProfilePictureChange={handleProfilePictureChange}
        handleSaveProfilePicture={handleSaveProfilePicture}
      />
      <ChangePasswordModal
        showModal={showChangePasswordModal}
        handleCloseModal={handleCloseChangePasswordModal}
      />
    </Container>
  );
};

export default WorkInformation;
