import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import editIcon from "../../assets/others/editIcon.png";
import "./EmployeeInformationStyle.css";

const EmployeeInformation = () => {
  const employeeData = {
    birthDate: "01/01/1980",
    gender: "Male",
    maritalStatus: "Single",
    address: "123 Street",
    cityProvince: "City",
    districtMunicipality: "District",
    postalCode: "12345",
    sssNumber: "123-456-789",
    pagIbigNumber: "123-456-789",
    tinNumber: "123-456-789",
    philHealthNumber: "123-456-789",
    bankAccountNumber: "1234567890",
  };

  return (
    <Container fluid className="employee-information-container">
      <div className="employeeinfo-header">
        <h4 className="title">Personal Information</h4>
        <Image src={editIcon} alt="edit" className="edit-icon" />
      </div>
      <Row className="info-container">
        <Col md={3}>
          <div className="info-group">
            <div className="info-item">
              <label>Birth Date</label>
              <span>{employeeData.birthDate}</span>
            </div>
            <div className="info-item">
              <label>Address</label>
              <span>{employeeData.address}</span>
            </div>
            <div className="info-item">
              <label>SSS No.</label>
              <span>{employeeData.sssNumber}</span>
            </div>
            <div className="info-item">
              <label>PhilHealth No.</label>
              <span>{employeeData.philHealthNumber}</span>
            </div>
          </div>
        </Col>
        <Col md={3}>
          <div className="info-group">
            <div className="info-item">
              <label>Gender</label>
              <span>{employeeData.gender}</span>
            </div>
            <div className="info-item">
              <label>City/Province</label>
              <span>{employeeData.cityProvince}</span>
            </div>
            <div className="info-item">
              <label>Pag-IBIG No.</label>
              <span>{employeeData.pagIbigNumber}</span>
            </div>
            <div className="info-item">
              <label>Bank Acc. No.</label>
              <span>{employeeData.bankAccountNumber}</span>
            </div>
          </div>
        </Col>
        <Col md={4}>
          <div className="info-group">
            <div className="info-item">
              <label>Marital Status</label>
              <span>{employeeData.maritalStatus}</span>
            </div>
            <div className="info-item">
              <label>District/Municipality</label>
              <span>{employeeData.districtMunicipality}</span>
            </div>
            <div className="info-item">
              <label>TIN No.</label>
              <span>{employeeData.tinNumber}</span>
            </div>
          </div>
        </Col>
        <Col md={2}>
          <div className="info-group">
            <div className="info-item">
              <div className="postal-item">
                <label>Postal</label>
                <span>{employeeData.postalCode}</span>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default EmployeeInformation;
