import React, { useState } from "react";
import { Image, Modal } from "react-bootstrap";
import questionImage from "../../assets/others/question.png";
import proceedImage from "../../assets/others/proceed.png";
import closeIcon from "../../assets/others/Cancel.png";
import replyIcon from "../../assets/others/Answer.png";
import "../../routes/nav-tabs/styles/FAQsStyle.css";

const FAQQuestions = ({
  question,
  answer1,
  answer2,
  answer3,
  answer4,
  subquestionTitle,
  subquestion,
  subanswer1,
  subanswer2,
  subanswer3,
  subanswer4,
}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  return (
    <div className="faq-container">
      <div className="rectangle-header" />
      <div className="faq-question" onClick={handleShowModal}>
        <Image src={questionImage} alt="Question" className="question-image" />
        <p>{question}</p>
        <Image src={proceedImage} alt="Answer" className="proceed-image" />
      </div>
      <Modal
        show={showModal}
        onHide={handleCloseModal}
        className="faqs-modal-container"
      >
        <div className="rectangle-header" />
        <Modal.Body>
          <div className="upper-content">
            <h3>Answer</h3>
            <img
              src={closeIcon}
              alt="Close"
              className="close-icon"
              onClick={handleCloseModal}
            />
          </div>

          <ul className="answer-content">
            <li>{question}</li>
            <div className="answer-container">
              <img src={replyIcon} alt="Reply" className="reply-icon" />
              <span className="answer">
                {answer1}
                <br></br>
                {answer2}
                <br></br>
                {answer3}
                <br></br>
                {answer4}
              </span>
            </div>
            {subquestion && (
              <ul className="subquestion-content">
                <h3 className="subquestion-title">{subquestionTitle}</h3>
                <li className="subquestion">{subquestion}</li>
                <div className="subanswer-container">
                  <img src={replyIcon} alt="Reply" className="reply-icon" />
                  <span className="subanswer">
                    {subanswer1}
                    <br></br>
                    {subanswer2}
                    <br></br>
                    {subanswer3}
                    <br></br>
                    {subanswer4}
                  </span>
                </div>
              </ul>
            )}
          </ul>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default FAQQuestions;
