import React from "react";
import { Modal, Button } from "react-bootstrap";
import "./SignoutConfirmationStyle.css";

const SignoutConfirmation = ({ show, onHide, onConfirm }) => {
  return (
    <React.Fragment>
      <Modal show={show} onHide={onHide} className="signout-modal-container">
        <div className="rectangle-header" />
        <Modal.Body className="modal-body">
          Are you sure you want to sign out?
        </Modal.Body>
        <Modal.Footer className="modal-footer">
          <Button
            variant="primary"
            onClick={onConfirm}
            className="modal-button sign-out"
          >
            Yes
          </Button>
          <Button variant="secondary" onClick={onHide} className="modal-button">
            No
          </Button>
        </Modal.Footer>
      </Modal>
    </React.Fragment>
  );
};

export default SignoutConfirmation;
