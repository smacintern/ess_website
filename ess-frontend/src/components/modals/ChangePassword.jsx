import React, { useState } from "react";
import "./ChangePasswordStyle.css";
import { Modal, Button, Form } from "react-bootstrap";
import closeIcon from "../../assets/others/Cancel.png";
import { useNavigate } from "react-router-dom";

const ChangePasswordModal = ({ showModal, handleCloseModal }) => {
  const [success, setSuccess] = useState(false);
  const navigate = useNavigate();

  const handleConfirmPassword = () => {
    setSuccess(true);
  };

  const handleSignIn = () => {
    navigate("/");
  };

  return (
    <Modal
      show={showModal}
      onHide={handleCloseModal}
      className="changepassword-modal-container"
    >
      <div className="rectangle-header" />
      <Modal.Header>
        <Modal.Title>Change Password</Modal.Title>
        {success ? null : (
          <img
            src={closeIcon}
            alt="Close"
            className="close-icon"
            onClick={handleCloseModal}
          />
        )}
      </Modal.Header>
      <Modal.Body>
        {success ? (
          <p>You've successfully changed your password. Please Sign In again</p>
        ) : (
          <Form>
            <div className="current-password">
              <Form.Group controlId="formBasicUsername">
                <Form.Control type="text" placeholder="Enter username" />
              </Form.Group>
              <Form.Group controlId="formBasicCurrentPassword">
                <Form.Control
                  type="password"
                  placeholder="Enter current password"
                />
              </Form.Group>
            </div>
            <div className="new-password">
              <Form.Group controlId="formBasicNewPassword">
                <Form.Control
                  type="password"
                  placeholder="Enter new password"
                />
              </Form.Group>
              <Form.Group controlId="formBasicConfirmPassword">
                <Form.Control
                  type="password"
                  placeholder="Confirm new password"
                />
              </Form.Group>
            </div>
          </Form>
        )}
      </Modal.Body>
      <Modal.Footer>
        {success ? (
          <Button variant="primary" className="back-to-signin"onClick={handleSignIn}>
            Sign In
          </Button>
        ) : (
          <>
            <Button variant="secondary" onClick={handleCloseModal}>
              Close
            </Button>
            <Button
              variant="primary"
              className="confirm-button"
              onClick={handleConfirmPassword}
            >
              Confirm
            </Button>
          </>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default ChangePasswordModal;
