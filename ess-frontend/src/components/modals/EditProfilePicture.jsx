import React, { useState } from "react";
import "./EditProfilePictureStyle.css";
import { Modal, Button, ModalFooter } from "react-bootstrap";
import closeIcon from "../../assets/others/Cancel.png";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

const EditProfilePicture = ({
  showModal,
  handleCloseModal,
  profilePicture,
  handleProfilePictureChange,
  handleSaveProfilePicture,
}) => {
  const [crop, setCrop] = useState({ aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = useState(null);

  return (
    <Modal
      show={showModal}
      onHide={handleCloseModal}
      className="editprofile-modal-container"
    >
      <div className="rectangle-header" />
      <Modal.Header>
        <Modal.Title>Customize Profile Picture</Modal.Title>
        <img
          src={closeIcon}
          alt="Close"
          className="close-icon"
          onClick={handleCloseModal}
        />
      </Modal.Header>
      <Modal.Body>
        <div className="editprofile-modal-content">
          <div className="left-section">
            <div className="profile-preview-container">
            <ReactCrop
              src={profilePicture}
              crop={crop}
              onChange={(newCrop) => setCrop(newCrop)}
              onComplete={(crop) => setCompletedCrop(crop)}
            />
            </div>
          </div>

          <div className="right-section">
            <div className="profile-preview-container">
              <img
                src={profilePicture}
                alt="Profile Preview"
                className="profile-preview"
              />
            </div>
            <input
              type="file"
              accept="image/*"
              onChange={handleProfilePictureChange}
            />
          </div>
        </div>
      </Modal.Body>
      <ModalFooter>
        <Button variant="primary" onClick={handleSaveProfilePicture}>
          Save
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EditProfilePicture;
