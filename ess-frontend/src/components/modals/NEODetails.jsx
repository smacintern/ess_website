import React from "react";
import { Modal } from "react-bootstrap";
import "./NEODetailsStyle.css";
import closeIcon from "../../assets/others/Cancel.png";

const NEOModal = ({ show, onClose }) => {
  return (
    <>
      <Modal show={show} onHide={onClose} className="neo-modal-container">
        <div className="rectangle-header" />
        <Modal.Body>
          <div className="neo-modal-content">
            <div className="upper-content">
              <h3>Employee self-service (ESS)</h3>
              <img
                src={closeIcon}
                alt="Close"
                className="close-icon"
                onClick={onClose}
              />
            </div>

            <ul>
              <li>
                A <span class="highlight">web-based application</span> that
                provides employees' access to their
                <span class="highlight"> personal records</span> as well with
                work details. Its most common feature is allowing the employees
                to change their <span class="highlight">personal records </span>
                and <span class="highlight">information</span> as well the
                ability to
                <span class="highlight"> apply for leave</span>,{" "}
                <span class="highlight">official business</span>, and
                <span class="highlight"> overtime applications</span> to which
                is directed to the employee's manager for approval. Some
                applications enabled the employees to{" "}
                <span class="highlight">view </span>
                their own <span class="highlight">daily time record</span>.
              </li>
            </ul>

            <h3>Equitable Computer Services, Inc. (ECS)</h3>
            <ul>
              <li>
                One of the most progressive{" "}
                <span class="highlight">computer solutions providers</span> in
                the country. It offers a broad spectrum of interrelated hardware
                and software services suited to fulfill the ever-increasing
                demands of today's fast paced IT market.
                <br />
                <br />
                ECS was established in{" "}
                <span class="highlight">February 22, 1973</span> and has since
                been at the forefront of innovation across various vertical
                markets providing turn-key systems, online and batch computer
                processing, unique products, including dedicated and
                professional technical support. The company continues to broaden
                its coverage in software development, systems integration,
                facilities management, point-of-sale (POS) maintenance services,
                call center, and technical support services, among others.
                <br />
                <br />
                Initially created to serve the IT needs of Equitable Bank, ECS
                is now considered as one of the Philippine's leading computer
                service providers. Its superior{" "}
                <span class="highlight">end-to-end card processing</span> and
                <span class="highlight"> POS maintenance services</span> were
                instrumental in the phenomenal growth of Equitable Card Network,
                Inc. (ECN) from 1989 to 2005.
                <br />
                <br />
              </li>
              <li>
                To date, ECS provides outsourced authorization services for the
                <span class="highlight"> four major card brands</span> - VISA,
                MasterCard, American Express, and JCB. The company is also{" "}
                <span class="highlight">
                  one of the few distributors of Hypercom
                </span>
                , a leading global provider of electronic payment solutions in
                the country, and regarded as a pioneer in the{" "}
                <span class="highlight">delivery of POS services</span>. ECS
                will soon join the ranks of companies that have earned VISA
                International's certification for third-party card service
                providers. With over <span class="highlight">200</span> highly
                qualified and trained IT professionals, ECS is committed to
                provide excellent service to its customers 24 hours a day, 7
                days a week. By keeping its workforce abreast of the latest
                trends and advancements in technology, ECS guarantees to
                continuously offer top-of-the-line products and unsurpassed
                service to its valued customers nationwide.
              </li>
            </ul>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default NEOModal;
