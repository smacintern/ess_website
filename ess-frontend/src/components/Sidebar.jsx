import React, { useState } from "react";
import { Image, Nav, Navbar } from "react-bootstrap";
import profilePicture from "../assets/sidebar-icons/user.png";
import companyLogo from "../assets/logo/ECS_Logo.png";
import collapseIcon from "../assets/sidebar-icons/collapse.png";
import attendanceIcon from "../assets/sidebar-icons/Approval.png";
import dailyTimeRecordIcon from "../assets/sidebar-icons/Schedule.png";
import viewListsIcon from "../assets/sidebar-icons/Bullet_List.png";
import requestIcon from "../assets/sidebar-icons/Email.png";
import dropdownIcon from "../assets/sidebar-icons/drop-down.png";
import formsIcon from "../assets/sidebar-icons/Google_Forms.png";
import administratorIcon from "../assets/sidebar-icons/administrator.png";
import "./SidebarStyle.css";
import "bootstrap/dist/css/bootstrap.min.css";

const Sidebar = () => {
  const [showSubMenu, setShowSubMenu] = useState(false);
  const [collapseSidebar, setCollapseSidebar] = useState(false);

  const toggleSubMenu = () => {
    setShowSubMenu(!showSubMenu);
  };

  const toggleCollapse = () => {
    setCollapseSidebar(!collapseSidebar);
  };

  return (
    <React.Fragment>
      <Navbar className={`sidebar ${collapseSidebar ? "collapsed" : ""}`}>
        <Image
          src={collapseIcon}
          className={`collapse-icon ${collapseSidebar ? "rotate" : ""}`}
          onClick={toggleCollapse}
        />
        <Navbar.Brand className="user-profile">
          <Nav.Link fluid href="/personal-information">
            <Image
              src={profilePicture}
              className="profile-picture"
              roundedCircle
            />
            <div className="user-details">
              <div className="user-name">John Doe</div>
              <div className="user-position">Software Engineer</div>
            </div>
          </Nav.Link>
        </Navbar.Brand>

        <Nav fluid className="flex-column">
          <Nav.Link fluid href="/attendance">
            <Image src={attendanceIcon} className="icon" />
            <span className="nav-text">Attendance</span>
          </Nav.Link>
          <Nav.Link href="/daily-time-record">
            <Image src={dailyTimeRecordIcon} className="icon" />
            <span className="nav-text">Daily Time Record</span>
          </Nav.Link>
          <Nav.Link href="/view-lists">
            <Image src={viewListsIcon} className="icon" />
            <span className="nav-text">View Lists</span>
          </Nav.Link>
          <Nav.Item className="submenu" onClick={toggleSubMenu}>
            <Image src={requestIcon} className="icon requests" />
            <span className="nav-text">Requests</span>
            <Image
              src={dropdownIcon}
              className={`dropdown-icon ${showSubMenu ? "open" : ""}`}
            />
            <div className={`submenu-items ${showSubMenu ? "show" : "hide"}`}>
              <Nav.Link href="/leave">Leave</Nav.Link>
              <Nav.Link href="/overtime">Overtime</Nav.Link>
              <Nav.Link href="/official-business">Official Business</Nav.Link>
              <Nav.Link href="/end-of-the-day">End of the Day</Nav.Link>
            </div>
          </Nav.Item>
          <Nav.Link href="/forms">
            <Image src={formsIcon} className="icon" />
            <span className="nav-text">Forms</span>
          </Nav.Link>
          <Nav.Link href="/administrator">
            <Image src={administratorIcon} className="icon" />
            <span className="nav-text">Administrator</span>
          </Nav.Link>
        </Nav>

        <Navbar.Brand className="company-logo">
          <Image src={companyLogo} className="logo" />
        </Navbar.Brand>
      </Navbar>
    </React.Fragment>
  );
};

export default Sidebar;
