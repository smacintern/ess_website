import React from "react";
import { Image, Container, Row, Col } from "react-bootstrap";
import doradoIcon from "../assets/logo/dorado.png";
import zohoIcon from "../assets/logo/zoho.png";
import "./FooterStyle.css";

const Footer = () => {
  return (
    <React.Fragment>
      <footer className="footer">
        <Container fluid>
          <Row>
            <Col
              xs={6}
              className="d-flex align-items-center justify-content-start"
            >
              <Image src={doradoIcon} className="footer-icon dorado-icon" />
              <div className="divider"></div>
              <Image src={zohoIcon} className="footer-icon zoho-icon" />
            </Col>
            <Col
              xs={6}
              className="d-flex align-items-center justify-content-end"
            >
              <p className="footer-text">
                © Equitable Computer Services, Inc. 2020
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    </React.Fragment>
  );
};

export default Footer;
