import React from "react";
import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import "./BannerCarouselStyle.css";
import img1 from "../../assets/others/highlights1.png";
import img2 from "../../assets/others/highlights2.png";
import img3 from "../../assets/others/highlights3.png";
import img4 from "../../assets/others/highlights4.png";
import img5 from "../../assets/others/highlights5.png";

const BannerCarousel = () => {
  return (
    <>
      <Carousel>
        <Carousel.Item>
          <img className="d-block w-100" src={img1} alt="First slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={img2} alt="Second slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={img3} alt="Third slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={img4} alt="Fourth slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={img5} alt="Fifth slide" />
        </Carousel.Item>
      </Carousel>
    </>
  );
};

export default BannerCarousel;
