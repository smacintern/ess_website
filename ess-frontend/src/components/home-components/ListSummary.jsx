import React from "react";
import { useNavigate } from "react-router-dom";
import { Tab, Tabs, Table, Button } from "react-bootstrap";
import "./ListSummaryStyle.css";

const ListSummary = () => {
  const navigate = useNavigate();

  const approvalRecords = [
    { documentType: "End of the Day Report", count: 2 },
    { documentType: "Overtime", count: 5 },
    { documentType: "Employee Leave", count: 1 },
    { documentType: "Official Business", count: 4 },
  ];

  const submittedRecords = [
    { documentType: "Employee Leave", date: "2024-04-27" },
    { documentType: "Official Business", date: "2024-06-20" },
    { documentType: "Overtime", date: "2024-08-15" },
  ];

  const handleViewAll = () => {
    navigate("/view-lists");
  };

  return (
    <>
      <div className="list-summary-header">
        <h2 className="list-summary-title ">List Summary</h2>
        <Button
          className="viewallButton"
          onClick={handleViewAll}
        >
          <span>View All</span>
        </Button>
      </div>
      <Tabs defaultActiveKey="approval" id="list-summary-tabs">
        <Tab eventKey="approval" title="Approval List" className="approval-tab">
          <Table striped bordered responsive className="custom-table">
            <thead>
              <tr>
                <th>Document Type</th>
                <th>Count</th>
              </tr>
            </thead>
            <tbody>
              {approvalRecords.map((record, index) => (
                <tr key={index}>
                  <td>{record.documentType}</td>
                  <td>{record.count}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Tab>
        <Tab
          eventKey="submitted"
          title="Submitted List"
          className="submitted-tab"
        >
          <Table striped bordered responsive className="custom-table">
            <thead>
              <tr>
                <th>Document Type</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {submittedRecords.map((record, index) => (
                <tr key={index}>
                  <td>{record.documentType}</td>
                  <td>{record.date}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Tab>
      </Tabs>
    </>
  );
};

export default ListSummary;
