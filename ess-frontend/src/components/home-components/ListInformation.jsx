import React, { useState } from "react";
import { Tab, Tabs, Table, Button } from "react-bootstrap";
import previousIcon from "../../assets/others/Previous.png";
import nextIcon from "../../assets/others/Next.png";
import "./ListInformationStyle.css";

const ListInformation = () => {
  const [selectedMonth, setSelectedMonth] = useState("January");
  const [startMonthIndex, setStartMonthIndex] = useState(0);

  const months = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  const handleNext = () => {
    if (startMonthIndex + 3 < months.length) {
      setStartMonthIndex(startMonthIndex + 3);
      setSelectedMonth(months[startMonthIndex + 3]);
    }
  };

  const handlePrevious = () => {
    if (startMonthIndex - 3 >= 0) {
      setStartMonthIndex(startMonthIndex - 3);
      setSelectedMonth(months[startMonthIndex - 3]);
    }
  };

  const handleMonthChange = (month) => {
    setSelectedMonth(month);
  };

  const filterRecords = (records) => {
    return records.filter((record) => {
      const date = new Date(record.date);
      return (
        date.toLocaleString("default", { month: "long" }) === selectedMonth
      );
    });
  };

  const holidayRecords = [
    { holiday: "Labor Day", date: "2024-05-01" },
    { holiday: "Mother's Day", date: "2024-05-12" },
    { holiday: "All Saint's Day", date: "2024-11-01" },
    { holiday: "New Year", date: "2024-01-01" },
    { holiday: "Christmas Eve", date: "2024-12-24" },
    { holiday: "Christmas Day", date: "2024-12-25" },
  ];

  const newHireRecords = [
    { name: "Alice", date: "2024-05-10" },
    { name: "Bob", date: "2024-05-15" },
    { name: "Charlie", date: "2024-05-20" },
    { name: "David", date: "2024-06-05" },
    { name: "Eva", date: "2024-01-12" },
  ];

  const leaveRecords = [
    { name: "Alice", date: "2024-05-05" },
    { name: "Bob", date: "2024-05-18" },
    { name: "Charlie", date: "2024-06-20" },
    { name: "David", date: "2024-01-08" },
  ];

  const attendanceRecords = [
    { name: "Alice", date: "2024-05-04" },
    { name: "Bob", date: "2024-05-15" },
    { name: "Charlie", date: "2024-05-20" },
    { name: "David", date: "2024-01-05" },
    { name: "Eva", date: "2024-07-12" },
  ];

  const filteredHolidayRecords = filterRecords(holidayRecords);
  const filteredNewHireRecords = filterRecords(newHireRecords);
  const filteredLeaveRecords = filterRecords(leaveRecords);
  const filteredAttendanceRecords = filterRecords(attendanceRecords);
  const endMonthIndex = Math.min(startMonthIndex + 3, months.length);
  const displayedMonths = months.slice(startMonthIndex, endMonthIndex);

  return (
    <>
      <Tabs defaultActiveKey="holiday" id="list-information-tabs">
        <Tab eventKey="holiday" title="Holiday" className="holiday-tab">
          <div className="d-flex justify-content-center align-items-center mb-1">
            <img
              src={previousIcon}
              onClick={handlePrevious}
              className="icon-button Previous"
              alt="Previous"
            />
            {displayedMonths.map((month, index) => (
              <Button
                className="monthsButton"
                key={index}
                onClick={() => handleMonthChange(month)}
                active={selectedMonth === month}
              >
                {month}
              </Button>
            ))}
            <img
              src={nextIcon}
              onClick={handleNext}
              className="icon-button Next"
              alt="Next"
            />
          </div>
          {filteredHolidayRecords.length > 0 ? (
            <Table striped bordered responsive className="custom-table">
              <thead>
                <tr>
                  <th>Holiday</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                {filteredHolidayRecords.map((record, index) => (
                  <tr key={index}>
                    <td>{record.holiday}</td>
                    <td>{record.date}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <p className="no-records-message">No records</p>
          )}
        </Tab>
        <Tab eventKey="new-hire" title="New Hire" className="new-hire-tab">
          <div className="d-flex justify-content-center align-items-center mb-1">
            <img
              src={previousIcon}
              onClick={handlePrevious}
              className="icon-button Previous"
              alt="Previous"
            />
            {displayedMonths.map((month, index) => (
              <Button
                className="monthsButton"
                key={index}
                onClick={() => handleMonthChange(month)}
                active={selectedMonth === month}
              >
                {month}
              </Button>
            ))}
            <img
              src={nextIcon}
              onClick={handleNext}
              className="icon-button Next"
              alt="Next"
            />
          </div>
          {filteredNewHireRecords.length > 0 ? (
            <Table striped bordered responsive className="custom-table">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Date Hired</th>
                </tr>
              </thead>
              <tbody>
                {filteredNewHireRecords.map((record, index) => (
                  <tr key={index}>
                    <td>{record.name}</td>
                    <td>{record.date}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <p className="no-records-message">No records</p>
          )}
        </Tab>
        <Tab eventKey="leave" title="Leave" className="leave-tab">
          <div className="d-flex justify-content-center align-items-center mb-1">
            <img
              src={previousIcon}
              onClick={handlePrevious}
              className="icon-button Previous"
              alt="Previous"
            />
            {displayedMonths.map((month, index) => (
              <Button
                className="monthsButton"
                key={index}
                onClick={() => handleMonthChange(month)}
                active={selectedMonth === month}
              >
                {month}
              </Button>
            ))}
            <img
              src={nextIcon}
              onClick={handleNext}
              className="icon-button Next"
              alt="Next"
            />
          </div>
          {filteredLeaveRecords.length > 0 ? (
            <Table striped bordered hover className="custom-table">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                {filteredLeaveRecords.map((record, index) => (
                  <tr key={index}>
                    <td>{record.name}</td>
                    <td>{record.date}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <p className="no-records-message">No records</p>
          )}
        </Tab>
        <Tab
          eventKey="attendance"
          title="Attendance"
          className="attendance-tab"
        >
          <div className="d-flex justify-content-center align-items-center mb-1">
            <img
              src={previousIcon}
              onClick={handlePrevious}
              className="icon-button Previous"
              alt="Previous"
            />
            {displayedMonths.map((month, index) => (
              <Button
                className="monthsButton"
                key={index}
                onClick={() => handleMonthChange(month)}
                active={selectedMonth === month}
              >
                {month}
              </Button>
            ))}
            <img
              src={nextIcon}
              onClick={handleNext}
              className="icon-button Next"
              alt="Next"
            />
          </div>
          {filteredAttendanceRecords.length > 0 ? (
            <Table striped bordered hover className="custom-table">
              <thead>
                <tr>
                  <th>Employee Name</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                {filteredAttendanceRecords.map((record, index) => (
                  <tr key={index}>
                    <td>{record.name}</td>
                    <td>{record.date}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <p className="no-records-message">No records</p>
          )}
        </Tab>
      </Tabs>
    </>
  );
};

export default ListInformation;
