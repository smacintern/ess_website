import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { Offcanvas } from "react-bootstrap";
import { Navbar as BootstrapNavbar, Nav, Button } from "react-bootstrap";
import { FaBars, FaSignOutAlt } from "react-icons/fa";
import SignoutConfirmation from "./modals/SignoutConfirmation";
import "./NavBar.css";

const Navbar = () => {
  const [showNav, setShowNav] = useState(false);
  const [showSignOutModal, setShowSignOutModal] = useState(false);
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const handleSignOut = () => {
    setShowSignOutModal(true);
  };

  const confirmSignOut = () => {
    setShowSignOutModal(false);
    navigate("/");
  };

  return (
    <React.Fragment>
      <BootstrapNavbar expand="lg" className="header">
        <BootstrapNavbar.Collapse id="basic-navbar-nav" className="nav-menu">
          <Nav className="mr-auto custom-nav">
            <Nav.Link
              as={Link}
              to="/home"
              className={`custom-link ${
                pathname === "/home" ? "current-page" : ""
              }`}
            >
              Home
            </Nav.Link>
            <Nav.Link
              as={Link}
              to="/neo"
              className={`custom-link ${
                pathname === "/neo" ? "current-page" : ""
              }`}
            >
              NEO
            </Nav.Link>
            <Nav.Link
              as={Link}
              to="/faqs"
              className={`custom-link ${
                pathname === "/faqs" ? "current-page" : ""
              }`}
            >
              FAQs
            </Nav.Link>
          </Nav>
          <Button className="sign-out-button" onClick={handleSignOut}>
            Sign Out <FaSignOutAlt size={15} style={{ marginLeft: "10px" }} />
          </Button>

          <SignoutConfirmation
            show={showSignOutModal}
            onHide={() => setShowSignOutModal(false)}
            onConfirm={confirmSignOut}
          />
        </BootstrapNavbar.Collapse>

        <BootstrapNavbar.Toggle
          aria-controls="basic-navbar-nav"
          className="hamburger"
          onClick={() => setShowNav((prev) => !prev)}
        >
          <FaBars size={20} style={{ color: "#000" }} />
        </BootstrapNavbar.Toggle>
      </BootstrapNavbar>

      <Offcanvas
        show={showNav}
        className="offcanvas"
        onHide={() => setShowNav(false)}
        placement="end"
        backdrop={true}
        scroll={false}
      >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Menu</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body className="offcanvas-body-text">
          <Nav className="flex-column">
            <Nav.Link
              as={Link}
              to="/home"
              onClick={() => setShowNav(false)}
              className={`custom-link ${
                pathname === "/home" ? "current-page" : ""
              }`}
            >
              Home
            </Nav.Link>
            <Nav.Link
              as={Link}
              to="/neo"
              onClick={() => setShowNav(false)}
              className={`custom-link ${
                pathname === "/neo" ? "current-page" : ""
              }`}
            >
              NEO
            </Nav.Link>
            <Nav.Link
              as={Link}
              to="/faqs"
              onClick={() => setShowNav(false)}
              className={`custom-link ${
                pathname === "/faqs" ? "current-page" : ""
              }`}
            >
              FAQs
            </Nav.Link>
          </Nav>
        </Offcanvas.Body>
      </Offcanvas>
    </React.Fragment>
  );
};

export default Navbar;
